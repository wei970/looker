var keys, map, heatmap, dataset, start, timerTick=1000;
var keyIndex=0, intensity=0;
const viz = {
    // Set up the initial state of the visualization
    create: function(element, config) {
      let options = ` <div id="options" style="font-size:20px;font-weight: bold;">Radius:<select id="radioOption" onChange="onSelectChange()"><option value="#">請選擇</option>`;
      for(let i=6; i<=70;i+=2){
        options = options + `<option value="${i}">${i}</option>`;
      }
      options= options + `</select></div>`;

      let timerOptions = ` <div id="options" style="font-size:20px;font-weight: bold;">Timer(ms):<select id="timerOption" onChange="onTimerSelectChange()"><option value="#">請選擇</option>`;
      for(let i=100; i<=5000;i+=100){
        timerOptions = timerOptions + `<option value="${i}">${i}</option>`;
      }
      timerOptions= timerOptions + `</select></div>`;
      // console.log(options)
      element.innerHTML = `
      <div id="key" style="color:red;font-size:26px;font-weight: bold;"></div>
      ${timerOptions}
      ${options}
      <div id="intensity" style="font-size:20px;font-weight: bold;">
        <input type="text" id="maxIntensity"></input>
        <input type="button" onclick="setIntensity()" value="setIntensity"></input>
      </div>
      <div id="map" style="width:100%;height:100%;"></div>`;
    },

    // Render in response to the data or settings changing
    update: function(data, element, config, queryResponse) {
        console.log("data: ",data);
        //console.log(JSON.stringify(data));
        keys = Object.keys(data[0]["tobe_iot.count"]);
        map = new google.maps.Map(document.getElementById("map"), {
          zoom: 8,
          center: {
            lat: 23.97565,
            lng: 120.973882
          },
          mapTypeControl: true
        });
        heatmap = new google.maps.visualization.HeatmapLayer({
          map: map
        });
        heatmap.set("radius", 16);
        document.getElementById("radioOption").value=16;
        document.getElementById("timerOption").value=timerTick;
        dataset = getData(data, keys);
        setHeatMap();
    }
};
looker.plugins.visualizations.add(viz);

function getData(data, keys){
  let list=[];
  for(k=0; k<keys.length; k++){
    let heatmapData=new google.maps.MVCArray();
    for(i=0; i<data.length; i++){
      if(data[i]["tobe_iot.count"][keys[k]].value!==null){
        let weight = data[i]["tobe_iot.count"][keys[k]].value;
        heatmapData.push({location: new google.maps.LatLng(data[i]["tobe_iot.gps_location_round"].value[0], data[i]["tobe_iot.gps_location_round"].value[1]), weight: weight});
        intensity = (weight>intensity) ? weight: intensity;
      }
    }
    list.push(heatmapData)
  }
  heatmap.set("maxIntensity", intensity);
  document.getElementById("maxIntensity").value=intensity;
  console.log(list)
  console.log(JSON.stringify(list));
  return list;
}

function setHeatMap(timestamp){
  if (start === undefined){
    start = timestamp;
    document.getElementById("key").innerHTML=keys[keyIndex];
    heatmap.setData(dataset[keyIndex]);
  }
  else{
    let elapsed = timestamp - start;
    // console.log(elapsed);
    if(elapsed>timerTick){
      if(keyIndex==keys.length-1){
        keyIndex = 0;
      }
      else{
        keyIndex = keyIndex+1;
      }

      document.getElementById("key").innerHTML=keys[keyIndex];
      heatmap.setData(dataset[keyIndex]);
      start = timestamp;
    }
  }
 
  // console.log(keys[keyIndex]);
  if(keys.length>1){
    window.requestAnimationFrame(setHeatMap);
  }
}

function onSelectChange() {
  var x = document.getElementById("radioOption").value;
  heatmap.set("radius", parseInt(x));
}

function onTimerSelectChange() {
  timerTick = parseInt(document.getElementById("timerOption").value);
}

function setIntensity(){
  var x = document.getElementById("maxIntensity").value;
  heatmap.set("maxIntensity", parseInt(x));
  console.log("maxIntensity: " + heatmap.get('maxIntensity'));
}