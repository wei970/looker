var keys, map, heatmap, objKey;
var keyIndex=0, intensity=0;
const viz = {
    // Set up the initial state of the visualization
    create: function(element, config) {  
      let options = ` <div id="options" style="font-size:20px;font-weight: bold;">Radius:<select id="radioOption" onChange="onSelectChange()"><option value="#">請選擇</option>`;
      for(let i=6; i<=70;i+=2){
        options = options + `<option value="${i}">${i}</option>`;
      }
      options= options + `</select></div>`;
      element.innerHTML = `
      <div id="key" style="color:red;font-size:26px;font-weight: bold;"></div>
      ${options}
      <div id="intensity" style="font-size:20px;font-weight: bold;">
        <input type="text" id="maxIntensity"></input>
        <input type="button" onclick="setIntensity()" value="setIntensity"></input>
      </div>
      <div id="map" style="width:100%;height:100%;"></div>`;
    },

    // Render in response to the data or settings changing
    update: async function(data, element, config, queryResponse) {
        console.log("data: ",data);
        if("weight" in data[0]){
            keys = Object.keys(data[0]["weight"]);
            objKey = "weight";
        }
        else if("tobe_iot.count" in data[0]){
            keys = Object.keys(data[0]["tobe_iot.count"]);
            objKey = "tobe_iot.count";
        }
        console.log("keys: " + keys);
        console.log("objKey: " + objKey);
        map = new google.maps.Map(document.getElementById("map"), {
          zoom: 8,
          center: {
            lat: 23.97565,
            lng: 120.973882
          },
          mapTypeControl: true
        });
        heatmap = new google.maps.visualization.HeatmapLayer({
          map: map
        });
        heatmap.set("radius", 16);
        let dataset = getData(data, keys);
        setHeatMap(dataset, keys);
        console.log("maxIntensity: " + heatmap.get('maxIntensity'));
    }
};
looker.plugins.visualizations.add(viz);

function getData(data, keys){
  let list=[];
  for(k=0; k<keys.length; k++){
    let heatmapData=[];
    for(i=0; i<data.length; i++){
      if(data[i][objKey][keys[k]].value!=null){          
        let weight = data[i]["tobe_iot.count"][keys[k]].value;
        heatmapData.push({location: new google.maps.LatLng(data[i]["tobe_iot.gps_location"].value[0], data[i]["tobe_iot.gps_location"].value[1]), weight: weight});
        intensity = (weight>intensity) ? weight: intensity;
      }
    }
    list.push(heatmapData)
  }
  heatmap.set("maxIntensity", intensity);
  document.getElementById("maxIntensity").value=intensity;
  console.log(list)
  return list;
}

function setHeatMap(dataset, keys){
  if(keyIndex==keys.length-1){
    keyIndex = 0;
  }
  else{
    keyIndex = keyIndex+1;
  }
//   console.log(keys[keyIndex]);
  document.getElementById("key").innerHTML=keys[keyIndex];
  heatmap.setData(dataset[keyIndex]);

//   setTimeout(function() {
//     setHeatMap(dataset, keys);
//   }, 5000)
}

function onSelectChange() {
  var x = document.getElementById("radioOption").value;
  heatmap.set("radius", parseInt(x));
}

function setIntensity(){
  var x = document.getElementById("maxIntensity").value;
  heatmap.set("maxIntensity", parseInt(x));
  console.log("maxIntensity: " + heatmap.get('maxIntensity'));
}