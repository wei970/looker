var map, dataset, mapInfo={};
var color = ["#00FF7F", "#D2B48C", "#FFFF00", "#87CEEB", "#7B68EE", "#F5F5F5", "#808080"];
var tripIndex = 0;

var head  = document.getElementsByTagName('head')[0];
var link  = document.createElement('link');
link.rel = 'stylesheet';
link.type = 'text/css'; 
link.href = 'https://bb.githack.com/wei970/looker/raw/6b7d0e510f1d8b88d32edf4dbb313bf9f83c342c/popup.css';
head.appendChild(link);

const viz = {
    // Set up the initial state of the visualization
    create: function(element, config) {
        element.innerHTML = `
        <div id="key" style="color:red;font-size:26px;font-weight: bold;"></div>
        <div id="map" style="width:100%;height:100%;"></div>`;
    },

    // Render in response to the data or settings changing
    update: function(data, element, config, queryResponse) {
        console.log("data: ",data);
        dataset = data;
        map = new google.maps.Map(document.getElementById("map"), { 
            zoom: 13,
            // center: {
            //   lat: 23.97565,
            //   lng: 120.973882
            // },
            mapTypeControl: true });
        handleMapInfo();
    }
};
looker.plugins.visualizations.add(viz);

function handleMapInfo(){
    let preDateTime = null;
    let tripId = 0;
    for(i=0; i<dataset.length; i+=6){
        let dateArray = dataset[i]["tobe_iotcardata.dtime_time"].value.split(' ');
        let date = dateArray[0].split('-');
        let time = dateArray[1].split(':');
        let dateTime = new Date(date[0], date[1], date[2], time[0], time[1], time[2]);
        if(preDateTime!=null){
            // Compare diff of datetime.
            let diff_seconds = parseInt((dateTime-preDateTime)/1000);
            // if diff>5 minutes
            if(diff_seconds>300){
                preDateTime=null;
                if(tripIndex==(color.length-1)){
                    tripIndex = 0;
                }
                else{
                    tripIndex++;
                }
                tripId = 0;
                console.log(tripIndex);
                console.log(color[tripIndex]);
            }
            else{
                tripId++;
            }
        }
        preDateTime = dateTime;
        // console.log(String.fromCharCode(tripIndex+65))
        // let marker = new google.maps.Marker({  
        //     // icon: colorIcon[tripIndex],
        //     label: String.fromCharCode(tripIndex+65) + tripId.toString(),
        //     map: map, 
        //     position: new google.maps.LatLng(dataset[i]["tobe_iotcardata.gps_location"].value[0], dataset[i]["tobe_iotcardata.gps_location"].value[1])  
        // });
        // map.setCenter(marker.getPosition());
        // map.setZoom(13);
        // marker.addListener("click", () => {
        //     infowindow.open(map, marker);
        // });
        // let label = i.toString() //+ ", "+ preDateTime.Format("HH:mm:ss");
        popup = new Popup(new google.maps.LatLng(dataset[i]["tobe_iotcardata.gps_location"].value[0], dataset[i]["tobe_iotcardata.gps_location"].value[1]),
        String.fromCharCode(tripIndex+65) + tripId.toString()); 
        popup.setMap(map);
        map.setCenter(popup.position);
        
        let infowindow = new google.maps.InfoWindow();
        let label = "<div id='label'>"+dataset[i]["tobe_iotcardata.dtime_time"].value+"</div>"; 
        infowindow.setContent(label);
        popup.addListener("click", () => {
            infowindow.open(map, popup);
        });
    }
}

class Popup extends google.maps.OverlayView {
    constructor(position, label) {
      super();
      this.position = position;
      
      const content = document.createElement("div");
      content.classList.add("popup-bubble");
      content.style.backgroundColor = color[tripIndex];
      content.innerHTML=label;
      
      const bubbleAnchor = document.createElement("div");
      bubbleAnchor.classList.add("popup-bubble-anchor");
      bubbleAnchor.style.backgroundColor = color[tripIndex];
      bubbleAnchor.appendChild(content); // This zero-height div is positioned at the bottom of the tip.

      this.containerDiv = document.createElement("div");
      this.containerDiv.classList.add("popup-container");
      this.containerDiv.appendChild(bubbleAnchor); // Optionally stop clicks, etc., from bubbling up to the map.

      Popup.preventMapHitsAndGesturesFrom(this.containerDiv);
    }
    /** Called when the popup is added to the map. */

    onAdd() {
      this.getPanes().floatPane.appendChild(this.containerDiv);
    }
    /** Called when the popup is removed from the map. */

    onRemove() {
      if (this.containerDiv.parentElement) {
        this.containerDiv.parentElement.removeChild(this.containerDiv);
      }
    }
    /** Called each frame when the popup needs to draw itself. */

    draw() {
      const divPosition = this.getProjection().fromLatLngToDivPixel(
        this.position
      ); // Hide the popup when it is far out of view.

      const display =
        Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000
          ? "block"
          : "none";

      if (display === "block") {
        this.containerDiv.style.left = divPosition.x + "px";
        this.containerDiv.style.top = divPosition.y + "px";
      }

      if (this.containerDiv.style.display !== display) {
        this.containerDiv.style.display = display;
      }
    }
  }